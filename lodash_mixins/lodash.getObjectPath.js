var _ = require('lodash'),
    util = require('util');
module.exports = exports = function(items, attrs) {
    var match = function(value) {
        for (var key in attrs) {
            if (attrs[key] !== (value || {})[key]) {
                return false;
            }
        }
        return true;
    };

    var traverse = function(value, path, parentIsArray) {
        var innerPath;

        _.forEach(value, function(val, ix) {
            if (match(val)) {
                if (parentIsArray) {
                    innerPath = path + '[' + ix + ']';
                } else {
                    innerPath = path + '.' + ix;
                }
                return false;
            }

            if (_.isObject(val) || _.isArray(val)) {
                if (!parentIsArray) {
                    innerPath = traverse(val, path + '.' + ix, _.isArray(val));
                } else {
                    innerPath = traverse(val, path + '[' + ix + ']', _.isArray(val));
                }
            }

            if (!!innerPath) {
                return false;
            }
        });

        return innerPath;
    };

    return traverse(items, '', false);

};
