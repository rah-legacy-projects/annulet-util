var _ = require('lodash'),
    util = require('util');
module.exports = exports = function(object, path) {
    if (!object) {
        return;
    }
    var parts = path.replace(/\[(\w+)\]/g, '.$1')
        .replace(/^\./, '')
        .split('.');
    while (parts.length) {
        var part = parts.shift();
        if (!!object[part]) {
            object = object[part];
        } else {
            return;
        }
    }
    return object;
}
