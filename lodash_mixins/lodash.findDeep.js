var _ = require('lodash');
module.exports = exports = function(items, attrs) {
    var match = function(value) {
        for (var key in attrs) {
            if (!_.isUndefined(value)) {
                if (/^[0-9a-fA-F]{24}$/.test((attrs[key] || '')
                        .toString()) &&
                    /^[0-9a-fA-F]{24}$/.test((value[key] || '')
                        .toString())) {
                    if (attrs[key].toString() !== value[key].toString()) {
                        return false;
                    }
                } else if (_.isFunction(attrs[key])) {
                    return attrs[key](key, value[key]);
                } else if (_.isRegExp(attrs[key])) {
                    return attrs[key].test(value[key]);
                } else if (attrs[key] !== value[key]) {
                    return false;
                }
            }
        }

        return true;
    };

    var traverse = function(value) {
        var result;

        _.forEach(value, function(val) {
            if (match(val)) {
                result = val;
                return false;
            }

            if (_.isObject(val) || _.isArray(val)) {
                result = traverse(val);
            }

            if (result) {
                return false;
            }
        });

        return result;
    };

    return traverse(items);

};
