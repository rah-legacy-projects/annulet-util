var _ = require('lodash');
module.exports = exports = function(obj, key, member) {
    if(!obj[key] && /^[0-9a-fA-F]{24}$/.test(obj.toString())){
        return obj.toString();
    }
    else if (!!obj[key] && /^[0-9a-fA-F]{24}$/.test(obj[key].toString())) {
        return obj[key].toString();
    } else if (_.isObject(obj[key])) {
        if (!!member && !!obj[key] && !!obj[key][member]) {
            return obj[key][member];
        } else {
            if (!!obj[key]._id) {
                return obj[key]._id.toString();
            }
            else if(!!obj[key].id){
                return obj[key].id.toString();
            }
        }
    }
    return null;
};
