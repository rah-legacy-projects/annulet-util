var _ = require('lodash'),
    util = require('util');
module.exports = exports = function(rootObject, object) {
    var path = _.getObjectPath(rootObject, object);
    if (!path) {
        return undefined;
    }
    var parts = path.replace(/\[(\w+)\]/g, '.$1')
        .replace(/^\./, '')
        .split('.');
    parts = parts.slice(0, parts.length - 1);
    var parentPath = parts.join('.');
    var parent = _.resolveObjectPath(rootObject, parentPath);
    return parent;
};
