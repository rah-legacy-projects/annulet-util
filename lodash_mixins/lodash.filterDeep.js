var _ = require('lodash');
module.exports = exports = function(items, attrs) {
    var match = function(value) {
        for (var key in attrs) {
            if (!!value) {
                if (/^[0-9a-fA-F]{24}$/.test((attrs[key] || '')
                        .toString()) &&
                    /^[0-9a-fA-F]{24}$/.test((value[key] || '')
                        .toString())) {
                    if (attrs[key].toString() !== value[key].toString()) {
                        return false;
                    }
                } else if (_.isFunction(attrs[key])) {
                    return attrs[key](key, value[key]);
                } else if (_.isRegExp(attrs[key])) {
                    return attrs[key].test(value[key]);
                } else if (attrs[key] !== value[key]) {
                    return false;
                }
            }
        }

        return true;
    };

    var results = [];
    var traverse = function(value) {
        _.forEach(value, function(val) {
            if (match(val)) {
                results.push(val);
            }

            if (_.isObject(val) || _.isArray(val)) {
                traverse(val);
            }
        });
    };

    traverse(items);
    return results;

};
