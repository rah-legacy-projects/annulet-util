var _ = require('lodash');
_.mixin(require('./lodash_mixins'));
_.mixin(require('./lodash_mixins'));
_.mixin(require('./lodash_mixins'));
_.mixin(require('./lodash_mixins'));


var parent = {
    array: [{
            a: 1
        }, {
            a: 2
        }, {
            a: 3
        },

    ],
    anotherArray: [{
        a: 5
    }, {
        a: 6
    }, {
        a: 7
    }, ],
};

var x = _.filterDeep(parent, {
    a: function(key, value) {
        console.log('\t\t' + key + ': ' + value);
        return (value % 2 == 1);
    }
});


console.log(x);
