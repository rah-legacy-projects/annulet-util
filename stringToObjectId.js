var mongoose = require('mongoose');

mongoose.Types.ObjectId.prototype.toObjectId = function() {
    return this;
}

String.prototype.toObjectId = function() {
    if (/^[0-9a-fA-F]{24}$/.test((this||'').toString())) {
        return new mongoose.Types.ObjectId(this.toString());
    }
    return null;
};
